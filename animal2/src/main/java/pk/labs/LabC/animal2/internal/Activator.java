package pk.labs.LabC.animal2.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;

/**
 * Created by Wojtek on 2015-01-18.
 */
public class Activator implements BundleActivator{
    private static BundleContext context;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        bundleContext.registerService(Animal.class.getName(),new Animal2(),null);
        context=bundleContext;
        Logger.get().log(this, "Animal2 przychodzi");
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        context=null;
        Logger.get().log(this, "Animal2 odchodzi");
    }
}
