package pk.labs.LabC.animal3.internal;

import pk.labs.LabC.contracts.Animal;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Created by Wojtek on 2015-01-18.
 */
public class Animal3 implements Animal {
    String species;
    String name;
    String status;
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public Animal3()
    {
        this.name="Animal 3";
        this.species="misie";
    }

    @Override
    public String getSpecies() {
        return this.species;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(String status) {
        this.status=status;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }
}
