package pk.labs.LabC.actions.internal;

import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;

/**
 * Created by Wojtek on 2015-01-18.
 */
public class Wstan implements AnimalAction {
    private String name;

    public Wstan()
    {
        this.name="Wstan";
    }

    public String getName()
    {
        return this.name;
    }
    @Override
    public boolean execute(Animal animal) {
        if (animal != null)
        {
            animal.setStatus("stoi");
            return true;
        }
        return false;
    }

    public String name(){
        return this.name;
    }
}