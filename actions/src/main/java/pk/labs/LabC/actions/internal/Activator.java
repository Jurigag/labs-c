package pk.labs.LabC.actions.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.AnimalAction;

import java.util.Hashtable;

/**
 * Created by Wojtek on 2015-01-18.
 */
public class Activator implements BundleActivator {
    private static BundleContext context;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        Hashtable prop1 = new Hashtable();
        Hashtable prop2 = new Hashtable();
        Hashtable prop3 = new Hashtable();
        prop1.put("species", new String[]{"pieski"});
        prop1.put("name", "Animal 1");
        prop2.put("species", new String[]{"kotki"});
        prop2.put("name", "Animal 2");
        prop3.put("species", new String[]{"misie"});
        prop3.put("name", "Animal 3");
        bundleContext.registerService(AnimalAction.class.getName(), new Poloz(), prop1);
        bundleContext.registerService(AnimalAction.class.getName(),new Wstan(),prop2);
        bundleContext.registerService(AnimalAction.class.getName(),new Usiadz(),prop3);
        context=bundleContext;
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        context=null;
    }
}
