package pk.labs.LabC.actions.internal;

        import pk.labs.LabC.contracts.Animal;
        import pk.labs.LabC.contracts.AnimalAction;

/**
 * Created by Wojtek on 2015-01-18.
 */
public class Poloz implements AnimalAction {
    private String name;

    public Poloz()
    {
        this.name="Wstan";
    }

    public String getName()
    {
        return this.name;
    }
    @Override
    public boolean execute(Animal animal) {
        if (animal != null)
        {
            animal.setStatus("lezy");
            return true;
        }
        return false;
    }

    public String name(){
        return this.name;
    }
}